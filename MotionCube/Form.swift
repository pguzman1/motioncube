//
//  Form.swift
//  MotionCube
//
//  Created by Patricio GUZMAN on 10/10/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit

class Form: UIView {
    
    var panGesture: UIPanGestureRecognizer?
    var pinchGesture: UIPinchGestureRecognizer?
    var rotateGesture: UIRotationGestureRecognizer?
    var firstX: CGFloat?
    var firstY: CGFloat?
    var VC: ViewController?
    var isCircle = false
    
    init(frame: CGRect, viewController: ViewController) {
        super.init(frame: frame)
        panGesture = UIPanGestureRecognizer()
        pinchGesture = UIPinchGestureRecognizer()
        rotateGesture = UIRotationGestureRecognizer()
        panGesture?.addTarget(self, action: #selector(didPan))
        pinchGesture?.addTarget(self, action: #selector(didPinch))
        rotateGesture?.addTarget(self, action: #selector(rotate))
        self.addGestureRecognizer(rotateGesture!)
        self.addGestureRecognizer(panGesture!)
        self.addGestureRecognizer(pinchGesture!)
        
        self.VC = viewController
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        if isCircle{
            return .ellipse
        }
        else {
            return .rectangle
        }
    }
    
    func didPan(_ sender: UIPanGestureRecognizer) {
        
        switch sender.state {
        case .began:
            VC?.gravityBehavior!.removeItem(self)
        case .changed:
            self.center = sender.location(in: VC?.view)
            VC?.animator!.updateItem(usingCurrentState: self)
        case .ended:
            VC?.gravityBehavior!.addItem(self)
        case .failed, .cancelled:
            print("failed or cancelled")
        case .possible:
            print("possible")
        }
    }
    
    func didPinch(_ sender: UIPinchGestureRecognizer) {
        switch sender.state {
        case .began:
            print("began")
            VC?.gravityBehavior!.removeItem(sender.view!)
            VC?.collision!.removeItem(sender.view!)
            VC?.behaviour!.removeItem(sender.view!)
        case .changed:
            print("changed")
            sender.view?.layer.bounds.size.height *= sender.scale
            sender.view?.layer.bounds.size.width *= sender.scale
            if let view = sender.view as? Form {
                if view.isCircle {
                    view.layer.cornerRadius *= sender.scale
                }
            }
            sender.scale = 1
        case .ended:
            print("ended")
            VC?.gravityBehavior!.addItem(sender.view!)
            VC?.behaviour!.addItem(sender.view!)
            VC?.collision!.addItem(sender.view!)
            VC?.animator!.updateItem(usingCurrentState: sender.view!)
        case .failed, .cancelled:
            print("failed or cancelled")
        case .possible:
            print("possible")
        }
    }
    
    func rotate(_ sender: UIRotationGestureRecognizer) {
        switch sender.state {
        case .began:
            print("began")
            VC?.gravityBehavior!.removeItem(self)
        case .changed:
            print("changed")
            if let gestureView = sender.view {
                gestureView.transform = CGAffineTransform(rotationAngle: sender.rotation)
            }
        case .ended:
            print("ended")
            VC?.gravityBehavior!.addItem(self)
            VC?.animator?.updateItem(usingCurrentState: self)
        case .failed, .cancelled:
            print("failed or cancelled")
        case .possible:
            print("possible")
        }
    }
    
}

