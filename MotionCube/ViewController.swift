//
//  ViewController.swift
//  MotionCube
//
//  Created by Patricio GUZMAN on 10/10/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    var animator: UIDynamicAnimator?
    var items: [UIDynamicItem] = []
    var collision: UICollisionBehavior?
    var behaviour: UIDynamicItemBehavior?
    var gravityBehavior: UIGravityBehavior?
    var myForms: [Form] = []
    var panGesture: UIPanGestureRecognizer?
    
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.animator = UIDynamicAnimator(referenceView: view)
        self.gravityBehavior = UIGravityBehavior()
        self.behaviour = UIDynamicItemBehavior(items: [])
        self.collision = UICollisionBehavior(items: [])
    }
    
    @IBAction func didTap(_ sender: UITapGestureRecognizer) {
        switch sender.state {
        case .ended:
            let formToadd = self.getNewForm(x: sender.location(in: view).x, y: sender.location(in: view).y)
            self.view.addSubview(formToadd)
            self.myForms.append(formToadd)
            formToadd.isUserInteractionEnabled = true
            formToadd.isMultipleTouchEnabled = true
            
            self.gravityBehavior?.addItem(formToadd)
            
            self.animator?.addBehavior(self.gravityBehavior!)
            
            self.items.append(formToadd)
            
            // Set collision
            self.collision?.addItem(formToadd)
            self.collision?.translatesReferenceBoundsIntoBoundary = true
            self.animator?.addBehavior(collision!)
            
            
            // Set elasticity
            behaviour?.addItem(formToadd)
            behaviour?.allowsRotation = true
            behaviour?.elasticity = 0.6
            self.animator?.addBehavior(behaviour!)
        case .cancelled, .failed:
            print("CANCEL OR FAILD")
        default:
            print("default")
        }
    }

    func getNewForm(x: CGFloat, y: CGFloat) -> Form {
        let newForm = Form(frame: CGRect(x: x - 50, y: y - 50, width: 100, height: 100), viewController: self)
        newForm.backgroundColor = .random()
        if Int(arc4random() % 2) == 1 {
            newForm.layer.cornerRadius = 50
            newForm.isCircle = true
        }
        newForm.clipsToBounds = true
        newForm.isUserInteractionEnabled = true
        newForm.isMultipleTouchEnabled = true
        
        
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        
        blurView.frame = newForm.bounds
        return newForm
    }
    
}
